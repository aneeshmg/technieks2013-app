package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class eventSix extends Activity implements OnClickListener
{
	Button reg6;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		String fp = "fonts/Nihilschiz Handwriting.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventsix);
		reg6 = (Button) findViewById(R.id.button6);
		reg6.setTypeface(tf);
		
		reg6.setOnClickListener(this);
	}
	public void onClick(View but1) {
        switch(but1.getId()) {

           case R.id.button6:
                                 Intent r6 = new Intent("com.technieks.app.REGFORM");
                                 startActivity(r6);
                                 break;

        }
	}


}
