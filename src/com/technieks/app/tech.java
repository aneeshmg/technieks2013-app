package com.technieks.app;

import android.app.Activity;

import android.os.Bundle;
import android.graphics.Typeface;
import android.widget.TextView;
public class tech extends Activity
{	TextView texttech;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	String fp = "fonts/handw.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tech);
        texttech = (TextView) findViewById(R.id.texttech);
        texttech.setTypeface(tf);
    }
}
