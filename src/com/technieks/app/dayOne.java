package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class dayOne extends Activity implements OnClickListener
{
	Button eventone,eventtwo,eventthree,eventfour,eventfive,eventsix,eventsixone;
	TextView t1,t2,t3,t4,t5,t6,t6_1;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	String fp = "fonts/ostrich-regular.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.listdayone);
        eventone = (Button) findViewById(R.id.event1);
        eventtwo = (Button) findViewById(R.id.event2);
        eventthree = (Button) findViewById(R.id.event3);
        eventfour = (Button) findViewById(R.id.event4);
        eventfive = (Button) findViewById(R.id.event5);
        eventsix = (Button) findViewById(R.id.event6);
        eventsixone = (Button) findViewById(R.id.event6_1);
        
        t1 = (TextView) findViewById(R.id.t1);     
        t2 = (TextView) findViewById(R.id.t2);
        t3 = (TextView) findViewById(R.id.t3);
        t4 = (TextView) findViewById(R.id.t4);
        t5 = (TextView) findViewById(R.id.t5);
        t6 = (TextView) findViewById(R.id.t6);
        t6_1 = (TextView) findViewById(R.id.t6_1);// eventsix = (Button) findViewById(R.id.event6);
        
        eventone.setOnClickListener(this);
        eventtwo.setOnClickListener(this);
        eventthree.setOnClickListener(this);
        eventfour.setOnClickListener(this);
        eventfive.setOnClickListener(this);
        eventsix.setOnClickListener(this);
        eventsixone.setOnClickListener(this);
        
        t1.setTypeface(tf);
        t2.setTypeface(tf);
        t3.setTypeface(tf);
        t4.setTypeface(tf);
        t5.setTypeface(tf);
        t6.setTypeface(tf);
        t6_1.setTypeface(tf);

       // eventsix.setOnClickListener(this);

    }
    public void onClick(View but1) {
        switch(but1.getId()) {

           case R.id.event1:
                                 Intent e1 = new Intent("com.technieks.app.EVENTONE");
                                 startActivity(e1);
                                 break;
           case R.id.event2:
                                 Intent e2 = new Intent("com.technieks.app.EVENTTWO");
                                 startActivity(e2);
                             break;
           case R.id.event3:
                                 Intent e3 = new Intent("com.technieks.app.EVENTTHREE");
                                 startActivity(e3);
                                break;
           case R.id.event4:
        	   					 Intent e4 = new Intent("com.technieks.app.EVENTFOUR");
        	   					 startActivity(e4);
        	   					 break;
           case R.id.event5:
               					Intent e5 = new Intent("com.technieks.app.EVENTFIVE");
               					startActivity(e5);
               					break;
          case R.id.event6:
               					Intent e6 = new Intent("com.technieks.app.EVENTSIX");
               					startActivity(e6);
               					break;
          case R.id.event6_1:
					            Intent e6_1 = new Intent("com.technieks.app.EVENTSIXONE");
					            startActivity(e6_1);
					            break;
               					
        }
    }
}
