package com.technieks.app;
 
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
 
public class Sponsors extends Activity {
  
	@Override
	public void onCreate(Bundle savedInstanceState) {
 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sponsors);
		
		ImageView ispon1 = (ImageView)findViewById(R.id.isponcor1);
		ImageView ispon2 = (ImageView)findViewById(R.id.isponcor2);
		ImageView ispon3 = (ImageView)findViewById(R.id.isponcor3);
		ImageView ispon4 = (ImageView)findViewById(R.id.isponcor4);
		ImageView ispon5 = (ImageView)findViewById(R.id.isponcor5);
		TextView tvspon4 = (TextView) findViewById(R.id.tvsponcor4);
		TextView tvspon1 = (TextView) findViewById(R.id.tvsponcor1);
		TextView tvspon2 = (TextView) findViewById(R.id.tvsponcor2);
		TextView tvspon3 = (TextView) findViewById(R.id.tvsponcor3);
		TextView tvspon5 = (TextView) findViewById(R.id.tvsponcor5);
		ispon1.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.myra.ac.in/"));
		        startActivity(intent);
		    }
		});
		ispon2.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.snapfitnessindia.com/"));
		        startActivity(intent);
		    }
		});
		ispon3.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.canarabank.com/English/Home.aspx"));
		        startActivity(intent);
		    }
		});
		ispon4.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.google.com "));
		        startActivity(intent);
		    }
		});
		
		ispon5.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.google.com "));
		        startActivity(intent);
		    }
		});
		String fp = "fonts/ostrich-regular.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fp);
        tvspon1.setTypeface(tf);
        tvspon2.setTypeface(tf);
        tvspon3.setTypeface(tf);
        tvspon4.setTypeface(tf);
        tvspon5.setTypeface(tf);
	}
 
}
