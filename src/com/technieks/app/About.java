package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.graphics.Typeface;
import android.widget.Button;

public class About extends Activity
{	Button day1,day2,day3;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {	
    	String fp = "fonts/Nihilschiz Handwriting.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.days);
        day1 = (Button) findViewById(R.id.day1);
        day2 = (Button) findViewById(R.id.day2);
        day3 = (Button) findViewById(R.id.day3);
        day1.setTypeface(tf);
        day2.setTypeface(tf);
        day3.setTypeface(tf);
        
        
        
    }
}
