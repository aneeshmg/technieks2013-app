package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;


public class Enter extends Activity implements OnClickListener
{
    Button dayone,daytwo,daythree;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.days);
        dayone = (Button) findViewById(R.id.day1);
        daytwo = (Button) findViewById(R.id.day2);
        daythree = (Button) findViewById(R.id.day3);
        dayone.setOnClickListener(this);
        daytwo.setOnClickListener(this);
        daythree.setOnClickListener(this);
        String fp = "fonts/ostrich-regular.ttf";
        String fp1 = "fonts/ostrich-regular.ttf";
        String fp2 = "fonts/ostrich-regular.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fp);
        Typeface tf1 = Typeface.createFromAsset(getAssets(), fp1);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), fp2);
        dayone.setTypeface(tf);
        daytwo.setTypeface(tf);
        daythree.setTypeface(tf);
        TextView hashmi = (TextView) findViewById(R.id.hashmi);
        hashmi.setTypeface(tf2);


    }
    public void onClick(View but) {
        switch(but.getId()) {

           case R.id.day1:
                                 Intent d1 = new Intent("com.technieks.app.DAYONE");
                                 startActivity(d1);
                                 break;
           case R.id.day2:
                                 Intent d2 = new Intent("com.technieks.app.DAYTWO");
                                 startActivity(d2);
                             break;
           case R.id.day3:
                                 Intent d3 = new Intent("com.technieks.app.DAYTHREE");
                                 startActivity(d3);
                                break;
        }
    }
}
