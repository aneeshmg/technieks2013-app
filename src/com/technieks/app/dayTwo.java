package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class dayTwo extends Activity implements OnClickListener
{
	Button eventseven,eventeight,eventnine; //eventten,eventeleven,eventtwelve,eventtwelveone,eventtwelvetwo;
	TextView t7,t8,t9;//t10,t11,t12,t12_1,t12_2;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	String fp = "fonts/ostrich-regular.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.listdaytwo);
        eventseven = (Button) findViewById(R.id.event7);
        eventeight = (Button) findViewById(R.id.event8);
        eventnine = (Button) findViewById(R.id.event9);
        //eventten = (Button) findViewById(R.id.event10);
        //eventeleven = (Button) findViewById(R.id.event11);
        //eventtwelve = (Button) findViewById(R.id.event12);
        //eventtwelveone = (Button) findViewById(R.id.event12_1);
        //eventtwelvetwo = (Button) findViewById(R.id.event12_2);
        
        t7 = (TextView) findViewById(R.id.t7);
        t8 = (TextView) findViewById(R.id.t8);
        t9 = (TextView) findViewById(R.id.t9);
        //t10 = (TextView) findViewById(R.id.t10);
        //t11 = (TextView) findViewById(R.id.t11);
        //t12 = (TextView) findViewById(R.id.t12);
        //t12_1 = (TextView) findViewById(R.id.t12_1);
        //t12_2 = (TextView) findViewById(R.id.t12_2);

        
        eventseven.setOnClickListener((android.view.View.OnClickListener) this);
        eventeight.setOnClickListener((android.view.View.OnClickListener) this);
        eventnine.setOnClickListener((android.view.View.OnClickListener) this);
     /*   eventten.setOnClickListener((android.view.View.OnClickListener) this);
        eventeleven.setOnClickListener((android.view.View.OnClickListener) this);
        eventtwelve.setOnClickListener((android.view.View.OnClickListener) this);
        eventtwelveone.setOnClickListener((android.view.View.OnClickListener) this);
        eventtwelvetwo.setOnClickListener((android.view.View.OnClickListener) this);*/

        t7.setTypeface(tf);
        t8.setTypeface(tf);
        t9.setTypeface(tf);
        /*t10.setTypeface(tf);
        t11.setTypeface(tf);
        t12.setTypeface(tf);
        t12_1.setTypeface(tf);
        t12_2.setTypeface(tf);*/
        
        
    }
    public void onClick(View but1) {
        switch(but1.getId()) {
        
        case R.id.event7:
				Intent e7 = new Intent("com.technieks.app.EVENTSEVEN");
				startActivity(e7);
				break;
case R.id.event8:
				Intent e8 = new Intent("com.technieks.app.EVENTEIGHT");
				startActivity(e8);
				break;
case R.id.event9:
				Intent e9 = new Intent("com.technieks.app.EVENTNINE");
				startActivity(e9);
				break;
/*case R.id.event10:
				Intent e10 = new Intent("com.technieks.app.EVENTTEN");
				startActivity(e10);
				break;
case R.id.event11:
				Intent e11 = new Intent("com.technieks.app.EVENTELEVEN");
				startActivity(e11);
				break;
case R.id.event12:
				Intent e12 = new Intent("com.technieks.app.EVENTTWELVE");
				startActivity(e12);
				break;
case R.id.event12_1:
	Intent e12_1 = new Intent("com.technieks.app.EVENTTWELVEONE");
	startActivity(e12_1);
	break;
case R.id.event12_2:
	Intent e12_2 = new Intent("com.technieks.app.EVENTTWELVETWO");
	startActivity(e12_2);
	break;
		*/
        }
    }
        
}
