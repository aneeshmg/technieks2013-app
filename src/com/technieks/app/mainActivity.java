package com.technieks.app;

import java.util.Date;
import java.text.DateFormat;
import java.lang.Object;

import android.app.Activity;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class mainActivity extends Activity implements OnClickListener
{
    Button enter,sponcors,liveband,about,tek;
    //TextView tym;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        enter = (Button) findViewById(R.id.enter);
        sponcors = (Button) findViewById(R.id.sponcors);
        liveband = (Button) findViewById(R.id.liveband);
        tek = (Button) findViewById(R.id.tekx);
        about = (Button) findViewById(R.id.about);
        
        enter.setOnClickListener(this);
        sponcors.setOnClickListener(this);
        liveband.setOnClickListener(this);
        about.setOnClickListener(this);
        tek.setOnClickListener(this);
        
        String fp = "fonts/ostrich-regular.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fp);
        
        enter.setTypeface(tf);
        about.setTypeface(tf);
        liveband.setTypeface(tf);
        sponcors.setTypeface(tf);

    }
    public void onClick(View but) {
        switch(but.getId()) {

           case R.id.enter:
                                 Intent ac = new Intent("com.technieks.app.ENTER");
                                 startActivity(ac);
                                 break;
           case R.id.sponcors:
                                 Intent bu = new Intent("com.technieks.app.SPONSORS");
                                 startActivity(bu);
                             break;
           case R.id.liveband:
               					Intent  lb = new Intent("com.technieks.app.LIVEBAND");
               					startActivity(lb);
               					break;
           case R.id.about:
                                 Intent cs = new Intent("com.technieks.app.TECH");
                                 startActivity(cs);
                                break;

           case R.id.tekx:
                                 Intent tx = new Intent("com.technieks.app.TEKX");
                                 startActivity(tx);
                                break;                     
        }
    }

}
