package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.media.MediaPlayer;

public class Splash extends Activity
{
    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle bg)
    {
        super.onCreate(bg);
        setContentView(R.layout.splash);
        MediaPlayer Intro = MediaPlayer.create(Splash.this, R.raw.intro);
        Intro.start();
        Thread timer = new Thread() {
           public void run() {
              try {
                 sleep(2500);
              } catch(InterruptedException e) {
                 e.printStackTrace();
              } finally {
                 Intent o = new Intent("com.technieks.app.MAINACTIVITY");
                 startActivity(o);
              }
           }
        };
        timer.start();
    }
    @Override
    protected void onPause() {
       super.onPause();
       finish();
    }
}
