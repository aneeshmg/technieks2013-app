package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class eventSixOne extends Activity implements OnClickListener
{
	Button reg6_1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		String fp = "fonts/Nihilschiz Handwriting.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventsixone);
		reg6_1 = (Button) findViewById(R.id.button6_1);
		reg6_1.setTypeface(tf);
		
		reg6_1.setOnClickListener(this);
	}
	public void onClick(View but1) {
        switch(but1.getId()) {

           case R.id.button6_1:
                                 Intent r61 = new Intent("com.technieks.app.REGFORM");
                                 startActivity(r61);
                                 break;

        }
	}


}
