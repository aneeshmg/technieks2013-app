package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class dayThree extends Activity implements OnClickListener
{
	Button eventfourteen,eventfifteen,eventsixteen,eventseventeen;//,eventeighteen;
	TextView t14,t15,t16,t17;//,t18;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    	String fp = "fonts/ostrich-regular.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.listdaythree);
      //  eventthirteen = (Button) findViewById(R.id.event13);
        eventfourteen = (Button) findViewById(R.id.event14);
        eventfifteen = (Button) findViewById(R.id.event15);
        eventsixteen = (Button) findViewById(R.id.event16);
        eventseventeen = (Button) findViewById(R.id.event17);
        //eventeighteen = (Button) findViewById(R.id.event18);
      //  t13= (TextView) findViewById(R.id.t13);
        t14= (TextView) findViewById(R.id.t14);
        t15= (TextView) findViewById(R.id.t15);
        t16= (TextView) findViewById(R.id.t16);
        t17= (TextView) findViewById(R.id.t17);
       // t18= (TextView) findViewById(R.id.t18);
        

       // eventthirteen.setOnClickListener((android.view.View.OnClickListener) this);
        eventfourteen.setOnClickListener((android.view.View.OnClickListener) this);
        eventfifteen.setOnClickListener((android.view.View.OnClickListener) this);
        eventsixteen.setOnClickListener((android.view.View.OnClickListener) this);
        eventseventeen.setOnClickListener((android.view.View.OnClickListener) this);
      //  eventeighteen.setOnClickListener((android.view.View.OnClickListener) this);

        //t13.setTypeface(tf);
        t14.setTypeface(tf);
        t15.setTypeface(tf);
        t16.setTypeface(tf);
        t17.setTypeface(tf);
      //  t18.setTypeface(tf);
    }
    public void onClick(View but1) {
        switch(but1.getId()) {

       
case R.id.event14:
				Intent e14 = new Intent("com.technieks.app.EVENTFOURTEEN");
				startActivity(e14);
				break;
case R.id.event15:
				Intent e15 = new Intent("com.technieks.app.EVENTFIFTEEN");
				startActivity(e15);
				break;
case R.id.event16:
	Intent e16 = new Intent("com.technieks.app.EVENTSIXTEEN");
	startActivity(e16);
	break;
case R.id.event17:
	Intent e17 = new Intent("com.technieks.app.EVENTSEVENTEEN");
	startActivity(e17);
	break;
/*case R.id.event18:
	Intent e18 = new Intent("com.technieks.app.EVENTEIGHTEEN");
	startActivity(e18);
	break;*/

}
}
}
