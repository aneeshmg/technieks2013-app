package com.technieks.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;


public class eventFive extends Activity implements OnClickListener
{
	Button reg5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		String fp = "fonts/Nihilschiz Handwriting.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventfive);
		reg5 = (Button) findViewById(R.id.button5);
		reg5.setTypeface(tf);
		
		reg5.setOnClickListener(this);
	}
	public void onClick(View but1) {
        switch(but1.getId()) {

           case R.id.button5:
                                 Intent r5 = new Intent("com.technieks.app.REGFORM");
                                 startActivity(r5);
                                 break;

        }
	}
  

}
