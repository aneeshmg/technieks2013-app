package com.technieks.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;


public class eventOne extends Activity implements OnClickListener
{
	Button reg1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		String fp = "fonts/Nihilschiz Handwriting.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventone);
		reg1 = (Button) findViewById(R.id.button1);
		reg1.setTypeface(tf);
		
		reg1.setOnClickListener(this);
	}
		
		public void onClick(View but1) {
	        Intent r1 = new Intent("com.technieks.app.REGFORM");
	                                 startActivity(r1);
	                                
		}

}
