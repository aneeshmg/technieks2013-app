package com.technieks.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;


public class eventFour extends Activity implements OnClickListener
{
	Button reg4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		String fp = "fonts/Nihilschiz Handwriting.ttf";
    	Typeface tf = Typeface.createFromAsset(getAssets(), fp);

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventfour);
		reg4 = (Button) findViewById(R.id.button4);
		reg4.setTypeface(tf);
		
		reg4.setOnClickListener(this);
	}
	public void onClick(View but1) {
        switch(but1.getId()) {

           case R.id.button4:
                                 Intent r4 = new Intent("com.technieks.app.REGFORM");
                                 startActivity(r4);
                                 break;

        }
	}

  

}
